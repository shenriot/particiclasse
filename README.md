# Particiclasse

**Particiclasse** est une application web destinée à favoriser la participation équitable des élèves en classe. Elle permet de désigner les élèves pour la prise de parole de manière aléatoire ou en fonction de leur participation, tout en suivant leur activité via un système de points. Cet outil est conçu pour aider les enseignants à gérer les discussions, encourager la participation, et suivre les interventions de chaque élève.

## Fonctionnalités

- **Désignation aléatoire d'élèves** : Sélectionnez un élève au hasard pour intervenir.
- **Suivi des points de participation** : Attribuez des points aux élèves pour chaque intervention.
- **Visualisation en temps réel** : Affichez les avatars et les scores de chaque élève en temps réel.
- **Classement des élèves** : Classez les élèves en fonction de leur participation.
- **Personnalisation des avatars** : Choisissez parmi plusieurs styles d'avatars pour représenter vos élèves.
- **Exportation et importation des données** : Exportez les résultats de la session ou importez une session précédente.

## Prérequis

- Un navigateur web moderne (Chrome, Firefox, Edge).
- Aucune installation nécessaire, il suffit de lancer l'application via le navigateur.

## Licence

Ce projet est sous licence Creative Commons Attribution-NonCommercial 4.0 International (CC BY-NC 4.0). Cela signifie que vous êtes libre de partager et d'adapter le matériel pour des utilisations non commerciales, tant que vous créditez l'œuvre et que vous n'appliquez pas de restrictions supplémentaires. Voir le fichier [LICENSE](LICENSE) pour plus de détails.

