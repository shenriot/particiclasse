let students = {};  // Stocker les informations sur les élèves
let scoreStarted = false;  // Indicateur pour savoir si les scores ont commencé

// Fonction pour sélectionner un élève au hasard et le mettre en avant
document.getElementById('randomSelectButton').addEventListener('click', () => {
    const studentIds = Object.keys(students);
    const randomId = studentIds[Math.floor(Math.random() * studentIds.length)];

    // Mettre en avant l'avatar sélectionné
    highlightSelectedStudent(randomId);

    // Afficher la modale avec les informations de l'élève
    showModal(randomId);
});

// Fonction pour afficher la modale avec l'élève sélectionné
function showModal(studentId) {
    const modal = document.getElementById('randomModal');
    const selectedAvatar = document.getElementById('selectedAvatar');
    const selectedName = document.getElementById('selectedName');
    
    // Récupérer l'avatar et le prénom de l'élève sélectionné
    const student = students[studentId];
    const avatar = document.getElementById(studentId).querySelector('img').cloneNode(true);
    
    // Afficher l'avatar et le nom dans la modale
    selectedAvatar.innerHTML = '';
    selectedAvatar.appendChild(avatar);
    selectedName.textContent = student.name;
    
    // Ouvrir la modale
    modal.classList.add('active');
}

// Fonction pour fermer la modale
document.getElementById('closeModalButton').addEventListener('click', () => {
    const modal = document.getElementById('randomModal');
    modal.classList.remove('active');
});

// Autres fonctions comme reset, génération d'avatars, etc. ...


// Fonction pour réinitialiser les scores
document.getElementById('resetButton').addEventListener('click', () => {
    Object.keys(students).forEach(studentId => {
        students[studentId].score = 0;
        document.getElementById(`score-badge-${studentId}`).textContent = `0`;
    });
    updateBadgeColors();  // Mettre à jour les couleurs après réinitialisation
    highlightUnderInterrogated();  // Appliquer la mise en avant après réinitialisation

    // Réactiver le bouton "Générer les avatars"
    document.getElementById('generateButton').disabled = false;
    document.getElementById('generateButton').classList.remove('opacity-50', 'cursor-not-allowed');
    scoreStarted = false;  // Réinitialiser l'indicateur des scores
});

// Variable pour définir le nombre d'élèves à mettre en valeur
const NUM_LOWEST_SCORES_TO_HIGHLIGHT = 3; // Met en valeur les 3 élèves ayant les scores les plus faibles

// Fonction pour générer les avatars et réinitialiser les scores
document.getElementById('generateButton').addEventListener('click', () => {
    const avatarStyle = document.getElementById('avatarStyle').value;
    const inputText = document.getElementById('nameInput').value;
    const studentContainer = document.getElementById('studentsContainer');
    studentContainer.innerHTML = '';

    // Séparer les prénoms et les trier par ordre alphabétique
    const names = inputText.split(',').map(name => name.trim()).sort((a, b) => a.localeCompare(b));

    names.forEach((name, index) => {
        const studentId = `student-${index}`;
        students[studentId] = {
            name: name,
            score: 0
        };

        // Créer un avatar
        const avatarDiv = document.createElement('div');
        avatarDiv.classList.add('avatarCard', 'm-4', 'cursor-pointer', 'text-center');
        avatarDiv.id = studentId;

        const avatarImg = document.createElement('img');
        const seed = Math.random().toString(36).substring(7);
        avatarImg.src = `https://api.dicebear.com/6.x/${avatarStyle}/svg?seed=${seed}`;
        avatarImg.alt = `Avatar de ${name}`;
        avatarImg.classList.add('w-24', 'h-24', 'mx-auto', 'rounded-full', 'border-4', 'border-gray-300', 'transition', 'duration-300');

        // Création du conteneur pour le prénom et le badge
        const nameAndScoreDiv = document.createElement('div');
        nameAndScoreDiv.classList.add('name-and-score');

        // Créer l'élément du prénom
        const nameLabel = document.createElement('p');
        nameLabel.textContent = name;
        nameLabel.classList.add('font-semibold');

        // Ajout du badge pour le score à côté du prénom
        const scoreBadge = document.createElement('div');
        scoreBadge.classList.add('score-badge');
        scoreBadge.textContent = students[studentId].score;
        scoreBadge.id = `score-badge-${studentId}`;

        // Ajouter le prénom et le badge dans le même conteneur
        nameAndScoreDiv.appendChild(nameLabel);
        nameAndScoreDiv.appendChild(scoreBadge);

        // Ajouter tout dans l'avatarDiv
        avatarDiv.appendChild(avatarImg);
        avatarDiv.appendChild(nameAndScoreDiv); 
        studentContainer.appendChild(avatarDiv);

        // Ajouter un clic pour augmenter le score
        avatarDiv.addEventListener('click', () => {
            students[studentId].score++;
            document.getElementById(`score-badge-${studentId}`).textContent = students[studentId].score;
            updateBadgeColors(); // Mettre à jour la couleur des badges après chaque clic
            highlightUnderInterrogated(); // Mettre en avant après chaque clic

            // Bloquer le bouton "Générer les avatars" dès qu'un score est mis à jour
            if (!scoreStarted) {
                scoreStarted = true;
                document.getElementById('generateButton').disabled = true;
                document.getElementById('generateButton').classList.add('opacity-50', 'cursor-not-allowed');
            }
        });
    });

    // Initialiser les couleurs des badges et la mise en avant au départ
    updateBadgeColors();
    highlightUnderInterrogated();
});


// Fonction pour mettre à jour les couleurs des badges en fonction du score
function updateBadgeColors() {
    const studentList = Object.keys(students).map(studentId => ({
        studentId,
        score: students[studentId].score
    }));

    const scores = studentList.map(student => student.score);
    const minScore = Math.min(...scores);
    const maxScore = Math.max(...scores);

    studentList.forEach(student => {
        const badge = document.getElementById(`score-badge-${student.studentId}`);
        badge.classList.remove('bg-red-500', 'bg-orange-500', 'bg-green-500');

        if (student.score === minScore && minScore !== maxScore) {
            badge.classList.add('bg-red-500');
        } else if (student.score === maxScore && maxScore !== minScore) {
            badge.classList.add('bg-green-500');
        } else {
            badge.classList.add('bg-orange-500');
        }
    });
}

// Fonction pour mettre en avant les élèves avec les scores les plus faibles
function highlightUnderInterrogated() {
    const studentList = Object.keys(students).map(studentId => ({
        studentId,
        score: students[studentId].score
    }));

    studentList.sort((a, b) => a.score - b.score);

    const lowestScoringStudents = studentList.slice(0, NUM_LOWEST_SCORES_TO_HIGHLIGHT);

    Object.keys(students).forEach(studentId => {
        const avatarDiv = document.getElementById(studentId).querySelector('img');
        avatarDiv.classList.remove('border-yellow-400', 'animate-pulse');
    });

    lowestScoringStudents.forEach(student => {
        const avatarDiv = document.getElementById(student.studentId).querySelector('img');
        avatarDiv.classList.add('border-yellow-400', 'animate-pulse'); // Ajouter la bordure jaune et l'effet de pulsation
    });
}

// Fonction pour mettre en avant un avatar sélectionné au hasard
function highlightSelectedStudent(studentId) {
    // Réinitialiser les bordures pour tous les élèves
    Object.keys(students).forEach(id => {
        const avatarDiv = document.getElementById(id).querySelector('img');
        avatarDiv.classList.remove('border-blue-500', 'scale-110', 'animate-pulse');
    });

    // Appliquer la mise en avant à l'avatar sélectionné
    const selectedAvatarDiv = document.getElementById(studentId).querySelector('img');
    selectedAvatarDiv.classList.add('border-blue-500', 'scale-110', 'animate-pulse');
}

// Fonction pour trier les avatars dans l'ordre de participation
document.getElementById('rankingButton').addEventListener('click', () => {
    const studentContainer = document.getElementById('studentsContainer');
    const studentList = Object.keys(students).map(studentId => ({
        studentId,
        score: students[studentId].score
    }));

    // Trier les élèves par score décroissant (du plus interrogé au moins interrogé)
    studentList.sort((a, b) => b.score - a.score);

    // Récupérer une copie des éléments DOM avant de les déplacer
    const avatarElements = studentList.map(student => document.getElementById(student.studentId));

    // Vider le conteneur pour réordonner
    studentContainer.innerHTML = '';

    // Réorganiser les avatars dans l'ordre des scores en réinsérant les éléments DOM
    avatarElements.forEach(avatarDiv => {
        studentContainer.appendChild(avatarDiv);  // Ajouter chaque avatar dans le nouvel ordre
    });
    
    // Optionnel : mettre en avant le meilleur élève
    if (studentList.length > 0) {
        highlightTopParticipant(studentList[0].studentId);
    }
});

// Fonction pour mettre en avant le meilleur participant (palmarès)
function highlightTopParticipant(studentId) {
    // Réinitialiser toutes les bordures
    Object.keys(students).forEach(id => {
        const avatarDiv = document.getElementById(id).querySelector('img');
        avatarDiv.classList.remove('border-gold', 'animate-pulse');
    });

    // Appliquer la mise en avant à l'avatar du meilleur participant
    const topAvatarDiv = document.getElementById(studentId).querySelector('img');
    topAvatarDiv.classList.add('border-gold', 'animate-pulse'); // Appliquer un effet de mise en avant
}

// Ajout d'une bordure dorée pour le meilleur participant
const style = document.createElement('style');
style.innerHTML = `
    .border-gold {
        border-color: #FFD700 !important; /* Or */
        border-width: 6px;
    }
`;
document.head.appendChild(style);




document.getElementById('helpButton').addEventListener('click', function() {
    introJs().setOptions({
        steps: [
            {
                intro: "Bienvenue sur Particiclasse ! Ce guide vous montrera comment utiliser l'application."
            },
            {
                element: document.querySelector('#nameInput'),
                intro: "Saisissez ici les prénoms des élèves, séparés par des virgules. Exemple : Lou, Anne, Julia."
            },
            {
                element: document.querySelector('#avatarStyle'),
                intro: "Choisissez un style d'avatar pour représenter les élèves."
            },
            {
                element: document.querySelector('#generateButton'),
                intro: "Cliquez sur ce bouton pour générer les avatars avec les prénoms que vous avez saisis."
            },
            {
                element: document.querySelector('#studentsContainer'),
                intro: "Les avatars des élèves apparaissent ici. Cliquez sur un avatar pour marquer qu'un élève a été sélectionné pour la prise de parole."
            },
            {
                element: document.querySelector('#randomSelectButton'),
                intro: "Utilisez ce bouton pour sélectionner aléatoirement un élève pour la prise de parole."
            },
            {
                element: document.querySelector('#resetButton'),
                intro: "Réinitialisez les scores de tous les élèves en utilisant ce bouton."
            },
            {
                element: document.querySelector('#rankingButton'),
                intro: "Cliquez ici pour afficher le palmarès des participations, indiquant les élèves qui ont pris la parole le plus souvent."
            }
        ],
        nextLabel: 'Suivant',
        prevLabel: 'Précédent',
        doneLabel: 'Terminer'
    }).start();
});
